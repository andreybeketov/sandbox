﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
	public class FinanceOperationEntity
	{
		public int ID { get; set; }

		public DateTime Date { get; set; }
		public decimal Amount { get; set; }
	}
}
