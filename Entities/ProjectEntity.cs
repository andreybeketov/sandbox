﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
	/// <summary>
	/// Сущность Проект
	/// </summary>
	[Table("Projects")]
	public class ProjectEntity
	{
		/// <summary>
		/// Ключевое поле
		/// </summary>
		public int ID { get; set; }

		/// <summary>
		/// Название проекта
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Описание проекта
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Дата создания проекта
		/// </summary>
		public DateTime DateCreated { get; set; }

		/// <summary>
		/// Планируемая дата выполнения проекта
		/// </summary>
		public DateTime? DateDuePlanned { get; set; }

		/// <summary>
		/// Фактическая дата выполнения проекта
		/// </summary>
		public DateTime? DateDueFact { get; set; }

		/// <summary>
		/// Статус проекта
		/// </summary>
		public StateType State { get; set; }

		/// <summary>
		/// Тип проекта: я делаю / мне делают
		/// </summary>
		public DirectionType Direction { get; set; }

		/// <summary>
		/// Финансовые поступления по проекту
		/// </summary>
		public ICollection<FinanceOperationEntity> IncomeFinance { get; set; }

		/// <summary>
		/// Задачи проекта
		/// </summary>
		public ICollection<TaskEntity> Tasks { get; set; }

		/// <summary>
		/// Теги для проекта
		/// </summary>
		public ICollection<TagEntity> Tags { get; set; }

	    /// <summary>
	    /// Идентификатор контрагента
	    /// </summary>
	    public int CounteragentID { get; set; }

	    /// <summary>
	    /// Навигационное свойство Контрагент
	    /// </summary>
	    public CounteragentEntity Counteragent { get; set; }
    }
}
